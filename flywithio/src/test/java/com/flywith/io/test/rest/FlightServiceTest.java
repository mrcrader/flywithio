package com.flywith.io.test.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.flywith.io.model.Flight;
import com.flywith.io.model.MediaType;
import com.flywith.io.rest.FlightService;
import com.flywith.io.rest.MediaService;
import com.flywith.io.service.MediaManager;
import com.flywith.io.service.MediaPath;
import com.flywith.io.util.MultivaluedHashMap;

@RunWith(Arquillian.class)
public class FlightServiceTest {
    
    @Deployment
    public static WebArchive deployment() {
        return RESTDeployment.deployment();
    }
   
    @Inject
    private FlightService flightService;
    
    @Inject
    private MediaService mediaService;
    
    @Inject
    private MediaManager mediaManager;
    
    @Test
    public void testGetFlightById() {
        
        // Test loading a single event
        Flight flight = flightService.getSingleInstance(1l);
        assertNotNull(flight);
        assertEquals("SFATEST", flight.getName());
        
    }
    
    @Test
    public void testGetFlightMedia() {
        
        // Test loading a single event
        Flight flight = flightService.getSingleInstance(1l);
        assertNotNull(flight);
        
        MediaPath path = mediaManager.getPath(flight.getMediaItem());
        assertNotNull(path);
        assertEquals(MediaType.IMAGE, path.getMediaType());
        
        assertNotNull(mediaService.getMediaContent(flight.getMediaItem().getId()));
    }
    
    @Test
    public void testPagination() {
        
        // Test pagination logic
        MultivaluedMap<String, String> queryParameters = new MultivaluedHashMap<String, String>();
        queryParameters.add("first", "2");
        queryParameters.add("maxResults", "1");
        
        List<Flight> flights = flightService.getAll(queryParameters);
        assertNotNull(flights);
        assertEquals(1, flights.size());
        assertEquals("Shane's Sock Puppets", flights.get(0).getName());
    }
    
    @Test
    public void testGetFlightsByCategory() {
        
        // Test getting events by venue
        MultivaluedMap<String, String> queryParameters = new MultivaluedHashMap<java.lang.String, java.lang.String>();
        
        queryParameters.add("category", "1");
        
        List<Flight> flights = flightService.getAll(queryParameters);
        assertNotNull(flights);
        assertEquals(5, flights.size());
        for (Flight e: flights) {
            assertEquals("Concert", e.getCategory().getDescription());
        }
    }

}
