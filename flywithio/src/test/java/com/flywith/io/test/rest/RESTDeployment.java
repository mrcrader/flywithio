package com.flywith.io.test.rest;

import org.jboss.shrinkwrap.api.spec.WebArchive;

import com.flywith.io.dto.VenueDto;
import com.flywith.io.model.Booking;
import com.flywith.io.rest.BaseEntityService;
import com.flywith.io.service.AllocatedSeats;
import com.flywith.io.service.MediaManager;
import com.flywith.io.service.MediaPath;
import com.flywith.io.service.SeatAllocationService;
import com.flywith.io.util.MultivaluedHashMap;
import com.flywiyh.io.test.FlywithioDeployment;

public class RESTDeployment {

    public static WebArchive deployment() {


        return FlywithioDeployment.deployment()
                .addPackage(Booking.class.getPackage())
                .addPackage(BaseEntityService.class.getPackage())
                .addPackage(VenueDto.class.getPackage())
                .addPackage(MultivaluedHashMap.class.getPackage())
                .addPackage(SeatAllocationService.class.getPackage())
                .addClass(AllocatedSeats.class)
                .addClass(MediaPath.class)
                .addClass(MediaManager.class);
    }
    
}
