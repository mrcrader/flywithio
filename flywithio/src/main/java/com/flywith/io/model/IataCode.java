package com.flywith.io.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 *	Visit https://www.nationsonline.org/oneworld/IATA_Codes/IATA_Code_A.htm to find more IATA codes
 *
 */

/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
@Entity
@Table(name="d_iata")
public class IataCode implements Serializable{

	 /* Declaration of fields */

    /**
     * The synthetic id of the object.
     */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	 /**
     * <p>
     * The airport IATA (International Air Transport Association) Code.
     * </p>
     * 
     * <p>
     * Visit {@link https://www.iata.org/publications/Pages/code-search.aspx } to find airport code.
     * <br>
     * Also link from Wikipedia for IATA defenition {@link https://en.wikipedia.org/wiki/International_Air_Transport_Association }
     * <br>
     * and for IATA Codes {@link https://en.wikipedia.org/wiki/IATA_airport_code }
     * </p>
     * 
     * <p>
     * The <code>@NotNull</code> Bean Validation constraint means that the IATA_CODE must be specified.
     * </p>
     */
	@Column(name = "IATA_CODE")
	@NotNull
	private String code;
	
	public IataCode() {
		
	}
	
	public Long getId() {
		return id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	/* toString(), equals() and hashCode() for IataCode, using the natural identity of the object */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IataCode other = (IataCode) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "IataCode [id=" + id + ", code=" + code + "]";
	}

}
