package com.flywith.io.model;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * <p>
 * Data about airplane
 * </p>
 * 
 * @author IBA Group
 * @since 2018
 */
/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
@Entity
public class Airplane implements Serializable {

    /* Declaration of fields */

    /**
     * The synthetic id of the object.
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    /**
     * <p>
     * The model of the airplane.
     * </p>
     * 
     * <p>
     * The model of the airplane forms it's natural identity and cannot be shared between events.
     * </p>
     * 
     * <p>
     * The model must not be null and must be one or more characters, the Bean Validation constrain <code>@NotEmpty</code>
     * enforces this.
     * </p>
     */
    @Column(unique = true)
    @NotEmpty
    private String model;
    
    @OneToOne
	private IcaoCode icaoCode;
	
	@ManyToOne
	private AeroCompany aeroCompany;

    /**
     * A description of the airplane
     */
    private String description;

    /**
     * <p>
     * A set of sections in the venue
     * </p>
     * 
     * <p>
     * The <code>@OneToMany<code> JPA mapping establishes this relationship. TODO Explain EAGER fetch. 
     * This relationship is bi-directional (a section knows which airplane it is part of), and the <code>mappedBy</code>
     * attribute establishes this. We cascade all persistence operations to the set of performances, so, for example if a airplane
     * is removed, then all of it's sections will also be removed.
     * </p>
     */
    @OneToMany(cascade = ALL, fetch = EAGER, mappedBy = "venue")
    private Set<Section> sections = new HashSet<Section>();

    /**
     * The capacity of the airplane
     */
    private int capacity;

    /**
     * An optional media item to entice punters to the airplane. The <code>@ManyToOne</code> establishes the relationship.
     */
    @ManyToOne
    private MediaItem mediaItem;

    /* Boilerplate getters and setters */
    public Long getId() {
		return id;
	}

	public String getModel() {
		return model;
	}

	public IcaoCode getIcaoCode() {
		return icaoCode;
	}

	public AeroCompany getAeroCompany() {
		return aeroCompany;
	}

	public String getDescription() {
		return description;
	}

	public Set<Section> getSections() {
		return sections;
	}

	public int getCapacity() {
		return capacity;
	}

	public MediaItem getMediaItem() {
		return mediaItem;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setIcaoCode(IcaoCode icaoCode) {
		this.icaoCode = icaoCode;
	}

	public void setAeroCompany(AeroCompany aeroCompany) {
		this.aeroCompany = aeroCompany;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setSections(Set<Section> sections) {
		this.sections = sections;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setMediaItem(MediaItem mediaItem) {
		this.mediaItem = mediaItem;
	}

    /* toString(), equals() and hashCode() for Venue, using the natural identity of the object */
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aeroCompany == null) ? 0 : aeroCompany.hashCode());
		result = prime * result + capacity;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((icaoCode == null) ? 0 : icaoCode.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mediaItem == null) ? 0 : mediaItem.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((sections == null) ? 0 : sections.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airplane other = (Airplane) obj;
		if (aeroCompany == null) {
			if (other.aeroCompany != null)
				return false;
		} else if (!aeroCompany.equals(other.aeroCompany))
			return false;
		if (capacity != other.capacity)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (icaoCode == null) {
			if (other.icaoCode != null)
				return false;
		} else if (!icaoCode.equals(other.icaoCode))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mediaItem == null) {
			if (other.mediaItem != null)
				return false;
		} else if (!mediaItem.equals(other.mediaItem))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (sections == null) {
			if (other.sections != null)
				return false;
		} else if (!sections.equals(other.sections))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return model;
    }
    
}
