package com.flywith.io.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * 
 * <p>
 * An information about an aerocompany.
 * </p>
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@Entity
@Table(name = "aerocompany")
@JsonIgnoreProperties("link")
public class AeroCompany {

	@Id
	@GeneratedValue
	private Long id;
	
	 /**
     * <p>
     * The aero company real name.
     * </p>
     * 
     * <p>
     * The <code>@NotNull</code> Bean Validation constraint means that the NAME must be specified.
     * </p>
     */
	@Size(min = 1, max = 25)
    @Pattern(regexp = "[^0-9]*", message = "Must not contain numbers")
	@NotNull
	private String name;
	
	/**
	 * Web link for official aero company site.
	 * Can be empty, but strongly recommend to insert for customers
	 */
	private String link;
	
	public AeroCompany() {
		
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AeroCompany other = (AeroCompany) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AeroCompany [id=" + id + ", name=" + name + ", link=" + link + "]";
	}
	
}
