package com.flywith.io.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>
 * Directory with ICAO Codes for airplanes
 * </p>
 * 
 * <p>
 * Read more about ICAO here: {@link https
 * ://en.wikipedia.org/wiki/International_Civil_Aviation_Organization }
 * </p>
 * 
 * @author IBA Group
 * @since 2018
 */
/*
 * We suppress the warning about not specifying a serialVersionUID, as we are
 * still developing this app, and want the JVM to generate the serialVersionUID
 * for us. When we put this app into production, we'll generate and embed the
 * serialVersionUID
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "d_icao")
/*
 * We indicate that some properties of the class shouldn't be marshalled in JSON
 * format
 */
@XmlRootElement
public class IcaoCode implements Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "ICAO_CODE")
	private String code;

	public IcaoCode() {

	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IcaoCode other = (IcaoCode) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IcaoCode [id=" + id + ", code=" + code + "]";
	}

}
