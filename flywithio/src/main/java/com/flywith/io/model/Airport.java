package com.flywith.io.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * Information about airport.
 * </p>
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name = "airport")
public class Airport implements Serializable{

	/* Declaration of fields */

    /**
     * The synthetic id of the object.
     */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	
	/**
     * <p>
     * The name of airport.
     * </p>
     * 
     * <p>
     * The <code>@NotNull</code> Bean Validation constraint means that the event must be specified.
     * </p>
     */
	@Column(name = "NAME")
	@NotNull
	private String name;
	
	/**
     * <p>
     * The airport IATA Code. Check the link bellow to find out what is IATA Code.
     * The <code>@ManyToOne<code> JPA mapping establishes this relationship.
     * </p>
     * 
     *{@link IataCode }
     * 
     * <p>
     * The <code>@NotNull</code> Bean Validation constraint means that the event must be specified.
     * </p>
     */
	@OneToOne
	@NotNull
	private IataCode iataCode;
	
	/**
     * <p>
     * The airport address.
     * </p>
     * 
     * 
     * <p>
     * The <code>@NotNull</code> Bean Validation constraint means that the event must be specified.
     * </p>
     * 
     * {@link Address }
     */
	@NotNull
	private Address address;
	
	public Airport() {
		
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public IataCode getIataCode() {
		return iataCode;
	}

	public Address getAddress() {
		return address;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIataCode(IataCode iataCode) {
		this.iataCode = iataCode;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	/* toString(), equals() and hashCode() for Airport, using the natural identity of the object */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((iataCode == null) ? 0 : iataCode.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airport other = (Airport) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (iataCode == null) {
			if (other.iataCode != null)
				return false;
		} else if (!iataCode.equals(other.iataCode))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Airport [id=" + id + ", name=" + name + ", iataCode=" + iataCode + ", address=" + address + "]";
	}
	
}
