package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import javax.ws.rs.core.UriBuilder;

import com.flywith.io.dto.AeroCompanyDto;
import com.flywith.io.model.AeroCompany;

/**
 * 
 * 	@author IBA Group
 * 	@since 2018
 *
 *	<p>A JAX-RS endpoint for handling {@link AeroCompany}s</p>
 *
 *	<p>In your named query, you have to use the name of the entity bean class, not the table name</p>
 *
 *	<p>For more information about createQuery click
 *	{@link https://developer.jboss.org/thread/111035 }
 *
 * 
 *	</p>
 *
 *	<p> To know why we used persist() over merge() visit:
 *	{@link https://stackoverflow.com/questions/1069992/jpa-entitymanager-why-use-persist-over-merge }
 *	</p>
 *
 */

@Stateless
@Path("/aerocompany")
public class AeroCompanyEndpoint {
   @PersistenceContext(unitName = "primary")
   private EntityManager em;
   
   @POST
   @Consumes("application/json")
   public Response create(AeroCompanyDto dto) {
      AeroCompany entity = dto.fromDto(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(AeroCompanyEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id) {
	   AeroCompany entity = em.find(AeroCompany.class, id);
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id) {
      TypedQuery<AeroCompany> findByIdQuery = em.createQuery("SELECT DISTINCT a FROM AeroCompany a WHERE a.id = :entityId ORDER BY a.id", AeroCompany.class);
      findByIdQuery.setParameter("entityId", id);
      AeroCompany entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      AeroCompanyDto dto = new AeroCompanyDto(entity);
      return Response.ok(dto).build();
   }

   @GET
   @Produces("application/json")
   public List<AeroCompanyDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
	  TypedQuery<AeroCompany> findAllQuery = em.createQuery("SELECT DISTINCT a FROM AeroCompany a ORDER BY a.id", AeroCompany.class);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<AeroCompany> searchResults = findAllQuery.getResultList();
      final List<AeroCompanyDto> results = new ArrayList<AeroCompanyDto>();
      for (AeroCompany searchResult : searchResults) {
    	  AeroCompanyDto dto = new AeroCompanyDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, AeroCompanyDto dto) {
      TypedQuery<AeroCompany> findByIdQuery = em.createQuery("SELECT DISTINCT a FROM AeroCompany a WHERE a.id = :entityId ORDER BY a.id", AeroCompany.class);
      findByIdQuery.setParameter("entityId", id);
      AeroCompany entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      entity = dto.fromDto(entity, em);
      try {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e) {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }

}
