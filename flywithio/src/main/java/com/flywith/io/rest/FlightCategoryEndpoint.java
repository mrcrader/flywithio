package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.flywith.io.dto.FlightCategoryDto;
import com.flywith.io.model.FlightCategory;

/**
 * 
 */
@Stateless
@Path("/flightcategories")
public class FlightCategoryEndpoint {
   @PersistenceContext(unitName = "primary")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public Response create(FlightCategoryDto dto) {
      FlightCategory entity = dto.fromDTO(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(FlightCategoryEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id) {
      FlightCategory entity = em.find(FlightCategory.class, id);
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id) {
      TypedQuery<FlightCategory> findByIdQuery = em.createQuery("SELECT DISTINCT f FROM FlightCategory f WHERE f.id = :entityId ORDER BY f.id", FlightCategory.class);
      findByIdQuery.setParameter("entityId", id);
      FlightCategory entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      FlightCategoryDto dto = new FlightCategoryDto(entity);
      return Response.ok(dto).build();
   }

   @GET
   @Produces("application/json")
   public List<FlightCategoryDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
      TypedQuery<FlightCategory> findAllQuery = em.createQuery("SELECT DISTINCT f FROM FlightCategory f ORDER BY f.id", FlightCategory.class);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<FlightCategory> searchResults = findAllQuery.getResultList();
      final List<FlightCategoryDto> results = new ArrayList<FlightCategoryDto>();
      for (FlightCategory searchResult : searchResults) {
         FlightCategoryDto dto = new FlightCategoryDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, FlightCategoryDto dto) {
      TypedQuery<FlightCategory> findByIdQuery = em.createQuery("SELECT DISTINCT f FROM FlightCategory f WHERE f.id = :entityId ORDER BY f.id", FlightCategory.class);
      findByIdQuery.setParameter("entityId", id);
      FlightCategory entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      entity = dto.fromDTO(entity, em);
      try {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e) {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }
   
}
