package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.flywith.io.dto.UserPasswordsDto;
import com.flywith.io.model.UserPassword;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 * In your named query, you have to use the name of the entity bean class, not the table name
 *
 *	<p>For more information about createQuery visit:
 *	{@link https://developer.jboss.org/thread/111035 }
 */

@Stateless
@Path("/userpasswords")
public class UserPasswordEndpoint {
   @PersistenceContext(unitName = "primary")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public Response create(UserPasswordsDto dto) {
      UserPassword entity = dto.fromDto(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(UserPasswordEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id) {
	   UserPassword entity = em.find(UserPassword.class, id);
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   //https://developer.jboss.org/thread/111035
   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id) {
      TypedQuery<UserPassword> findByIdQuery = em.createQuery("SELECT DISTINCT p FROM UserPassword p WHERE p.id = :entityId ORDER BY p.id", UserPassword.class);
      findByIdQuery.setParameter("entityId", id);
      UserPassword entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      UserPasswordsDto dto = new UserPasswordsDto(entity);
      return Response.ok(dto).build();
   }

   @GET
   @Produces("application/json")
   public List<UserPasswordsDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
      TypedQuery<UserPassword> findAllQuery = em.createQuery("SELECT p FROM UserPassword p ORDER BY p.id", UserPassword.class);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<UserPassword> searchResults = findAllQuery.getResultList();
      final List<UserPasswordsDto> results = new ArrayList<UserPasswordsDto>();
      for (UserPassword searchResult : searchResults) {
    	  UserPasswordsDto dto = new UserPasswordsDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, UserPasswordsDto dto) {
      TypedQuery<UserPassword> findByIdQuery = em.createQuery("SELECT DISTINCT p FROM UserPassword p WHERE p.id = :entityId ORDER BY p.id", UserPassword.class);
      findByIdQuery.setParameter("entityId", id);
      UserPassword entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      entity = dto.fromDto(entity, em);
      try {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e) {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }
}
