package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.flywith.io.dto.IcaoCodeDto;
import com.flywith.io.model.IcaoCode;

import javax.ws.rs.core.Response.Status;

/**
 * 
 * 	@author IBA Group
 * 	@since 2018
 *
 *	<p>A JAX-RS endpoint for handling {@link IcaoCode}s</p>
 *
 *	<p>In your named query, you have to use the name of the entity bean class, not the table name</p>
 *
 *	<p>For more information about createQuery
 *	<a href="https://developer.jboss.org/thread/111035">click</a>
 *	</p>
 *
 *	<p> To know why we used persist() over merge() visit:
 *	<a href="https://stackoverflow.com/questions/1069992/jpa-entitymanager-why-use-persist-over-merge">visit</a>
 *	</p>
 *
 */

@Stateless
@Path("/codes/icao")
public class IcaoCodeEndpoint {
   @PersistenceContext(unitName = "primary")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public Response create(IcaoCodeDto dto) {
      IcaoCode entity = dto.fromDto(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(IcaoCodeEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id) {
	   IcaoCode entity = em.find(IcaoCode.class, id);
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id)
   {
      TypedQuery<IcaoCode> findByIdQuery = em.createQuery("SELECT DISTINCT i FROM IcaoCode i WHERE i.id = :entityId ORDER BY i.id", IcaoCode.class);
      findByIdQuery.setParameter("entityId", id);
      IcaoCode entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      IcaoCodeDto dto = new IcaoCodeDto(entity);
      return Response.ok(dto).build();
   }

   @GET
   @Produces("application/json")
   public List<IcaoCodeDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
	  TypedQuery<IcaoCode> findAllQuery = em.createQuery("SELECT DISTINCT i FROM IcaoCode i ORDER BY i.id", IcaoCode.class);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<IcaoCode> searchResults = findAllQuery.getResultList();
      final List<IcaoCodeDto> results = new ArrayList<IcaoCodeDto>();
      for (IcaoCode searchResult : searchResults) {
    	  IcaoCodeDto dto = new IcaoCodeDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, IcaoCodeDto dto) {
      TypedQuery<IcaoCode> findByIdQuery = em.createQuery("SELECT DISTINCT i FROM IcaoCode i WHERE i.id = :entityId ORDER BY i.id", IcaoCode.class);
      findByIdQuery.setParameter("entityId", id);
      IcaoCode entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      entity = dto.fromDto(entity, em);
      try {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e) {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }

}
