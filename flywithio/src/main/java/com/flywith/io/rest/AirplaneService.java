package com.flywith.io.rest;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

import com.flywith.io.model.Airplane;

/**
 * <p>
 *     A JAX-RS endpoint for handling {@link Airplane}s. Inherits the actual
 *     methods from {@link BaseEntityService}.
 * </p>
 *
 * @author IBA Group
 * @since 2018
 */
@Path("/airplanes")
/**
 * <p>
 *     This is a stateless service, we declare it as an EJB for transaction demarcation
 * </p>
 */
@Stateless
public class AirplaneService extends BaseEntityService<Airplane> {

    public AirplaneService() {
        super(Airplane.class);
    }

}