package com.flywith.io.rest;

/**
 * An enumeration that represents the possible states for the Bot.
 * 
 * @author Vineet Reynolds
 * 
 */
public enum BotState {
    RUNNING, NOT_RUNNING, RESET
}
