package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import com.flywith.io.dto.AirportDto;
import com.flywith.io.model.Airport;

/**
 * 
 * 	@author IBA Group
 * 	@since 2018
 *
 *	<p>A JAX-RS endpoint for handling {@link Airport}s</p>
 *
 *	<p>In your named query, you have to use the name of the entity bean class, not the table name</p>
 *
 *	<p>For more information about createQuery click
 *	{@link https://developer.jboss.org/thread/111035 }
 *	</p>
 *
 *	<p> To know why we used persist() over merge() visit:
 *	{@link https://stackoverflow.com/questions/1069992/jpa-entitymanager-why-use-persist-over-merge }
 *	</p>
 *
 */

@Stateless
@Path("/airports")
public class AirportEndpoint {
   @PersistenceContext(unitName = "primary")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public Response create(AirportDto dto) {
      Airport entity = dto.fromDto(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(AirportEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id) {
	  Airport entity = em.find(Airport.class, id);
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id)
   {
      TypedQuery<Airport> findByIdQuery = em.createQuery("SELECT DISTINCT a FROM Airport a WHERE a.id = :entityId ORDER BY a.id", Airport.class);
      findByIdQuery.setParameter("entityId", id);
      Airport entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      if (entity == null) {
         return Response.status(Status.NOT_FOUND).build();
      }
      AirportDto dto = new AirportDto(entity);
      return Response.ok(dto).build();
   }

   @GET
   @Produces("application/json")
   public List<AirportDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult) {
	  TypedQuery<Airport> findAllQuery = em.createQuery("SELECT DISTINCT a FROM Airport a ORDER BY a.id", Airport.class);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Airport> searchResults = findAllQuery.getResultList();
      final List<AirportDto> results = new ArrayList<AirportDto>();
      for (Airport searchResult : searchResults) {
    	  AirportDto dto = new AirportDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, AirportDto dto) {
      TypedQuery<Airport> findByIdQuery = em.createQuery("SELECT DISTINCT a FROM Airport a WHERE e.id = :entityId ORDER BY a.id", Airport.class);
      findByIdQuery.setParameter("entityId", id);
      Airport entity;
      try {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre) {
         entity = null;
      }
      entity = dto.fromDto(entity, em);
      try {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e) {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }

}
