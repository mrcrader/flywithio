package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.flywith.io.model.Performance;
import com.flywith.io.model.Show;

/**
 * Metric data for a Show. Contains the identifier for the Show to identify it,
 * in addition to the flight name, the airplane name and capacity, and the metric
 * data for the performances of the Show.
 * 
 * @author IBA Group
 * @since 2018
 * 
 */
class ShowMetric {

	private Long show;
	private String flight;
	private String airplane;
	private int capacity;
	private List<PerformanceMetric> performances;

	// Constructor to populate the instance with data
	public ShowMetric(Show show, Map<Long, Long> occupiedCounts) {
		this.show = show.getId();
		this.flight = show.getFlight().getName();
		this.airplane = show.getVenue().getName();
		this.capacity = show.getVenue().getCapacity();
		this.performances = convertFrom(show.getPerformances(), occupiedCounts);
	}

	private List<PerformanceMetric> convertFrom(Set<Performance> performances,
			Map<Long, Long> occupiedCounts) {
		List<PerformanceMetric> result = new ArrayList<PerformanceMetric>();
		for (Performance performance : performances) {
			Long occupiedCount = occupiedCounts.get(performance.getId());
			result.add(new PerformanceMetric(performance, occupiedCount));
		}
		return result;
	}

	// Getters for Jackson
	// NOTE: No setters and default constructors are defined since
	// deserialization is not required.
	
	public Long getShow() {
		return show;
	}

	public String getFlight() {
		return flight;
	}

	public String getAirplane() {
		return airplane;
	}

	public int getCapacity() {
		return capacity;
	}

	public List<PerformanceMetric> getPerformances() {
		return performances;
	}
	
}
