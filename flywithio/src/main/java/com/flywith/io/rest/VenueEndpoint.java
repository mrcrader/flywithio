package com.flywith.io.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.flywith.io.dto.VenueDto;
import com.flywith.io.model.Venue;

/**
 * 
 */
@Stateless
@Path("fly/venues")
public class VenueEndpoint
{
   @PersistenceContext(unitName = "primary")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public Response create(VenueDto dto)
   {
      Venue entity = dto.fromDto(null, em);
      em.persist(entity);
      return Response.created(UriBuilder.fromResource(VenueEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public Response deleteById(@PathParam("id") Long id)
   {
      Venue entity = em.find(Venue.class, id);
      if (entity == null)
      {
         return Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public Response findById(@PathParam("id") Long id)
   {
      TypedQuery<Venue> findByIdQuery = em.createQuery("SELECT DISTINCT v FROM Venue v LEFT JOIN FETCH v.sections LEFT JOIN FETCH v.mediaItem WHERE v.id = :entityId ORDER BY v.id", Venue.class);
      findByIdQuery.setParameter("entityId", id);
      Venue entity;
      try
      {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre)
      {
         entity = null;
      }
      if (entity == null)
      {
         return Response.status(Status.NOT_FOUND).build();
      }
      VenueDto dto = new VenueDto(entity);
      return Response.ok(dto).build();
   }
   
   @GET
   @Path("aerocompany/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public List<VenueDto> findVenueByAerocompanyId(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult, @PathParam("id") Long id) {
	   
	   /**
	    * @return list of all venues which aerocompany own
	    */
	   
	  TypedQuery<Venue> findAllQuery = em.createQuery("SELECT DISTINCT v FROM Venue v WHERE v.aeroCompany.id = :entityId ORDER BY v.id", Venue.class);
	  findAllQuery.setParameter("entityId", id);
      if (startPosition != null) {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null) {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Venue> searchResults = findAllQuery.getResultList();
      final List<VenueDto> results = new ArrayList<VenueDto>();
      for (Venue searchResult : searchResults) {
    	  VenueDto dto = new VenueDto(searchResult);
         results.add(dto);
      }      return results;
   }

   @GET
   @Produces("application/json")
   public List<VenueDto> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
      TypedQuery<Venue> findAllQuery = em.createQuery("SELECT DISTINCT v FROM Venue v LEFT JOIN FETCH v.sections LEFT JOIN FETCH v.mediaItem ORDER BY v.id", Venue.class);
      if (startPosition != null)
      {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null)
      {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Venue> searchResults = findAllQuery.getResultList();
      final List<VenueDto> results = new ArrayList<VenueDto>();
      for (Venue searchResult : searchResults)
      {
         VenueDto dto = new VenueDto(searchResult);
         results.add(dto);
      }
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public Response update(@PathParam("id") Long id, VenueDto dto)
   {
      TypedQuery<Venue> findByIdQuery = em.createQuery("SELECT DISTINCT v FROM Venue v LEFT JOIN FETCH v.sections LEFT JOIN FETCH v.mediaItem WHERE v.id = :entityId ORDER BY v.id", Venue.class);
      findByIdQuery.setParameter("entityId", id);
      Venue entity;
      try
      {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre)
      {
         entity = null;
      }
      entity = dto.fromDto(entity, em);
      try
      {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e)
      {
         return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
      }
      return Response.noContent().build();
   }
}
