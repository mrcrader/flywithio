package com.flywith.io.dto;

import javax.persistence.EntityManager;

import com.flywith.io.model.UserPassword;

public class UserPasswordsDto {

	private Long id;
	private String password;
	
	public UserPasswordsDto() {
		
	}
	
	public UserPasswordsDto(UserPassword entity) {
		this.id = entity.getId();
		this.password = entity.getPassword();
	}
	
	public UserPassword fromDto(UserPassword entity, EntityManager em) {
		if(entity == null) {
			entity = new UserPassword();
		}
		entity.setPassword(this.password);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setPassword(final String password) {
		this.password = password;
	}
	
}
