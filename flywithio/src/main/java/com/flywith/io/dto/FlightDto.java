package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedFlightCategoryDto;
import com.flywith.io.dto.NestedMediaItemDto;
import com.flywith.io.model.Flight;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class FlightDto implements Serializable {

   private Long id;
   private String name;
   private NestedMediaItemDto mediaItem;
   private NestedFlightCategoryDto category;
   private String description;

   public FlightDto() {
	   
   }

   public FlightDto(final Flight entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.name = entity.getName();
         this.mediaItem = new NestedMediaItemDto(entity.getMediaItem());
         this.category = new NestedFlightCategoryDto(entity.getCategory());
         this.description = entity.getDescription();
      }
   }

   public Flight fromDTO(Flight entity, EntityManager em) {
      if (entity == null) {
         entity = new Flight();
      }
      entity.setName(this.name);
      if (this.mediaItem != null) {
         entity.setMediaItem(this.mediaItem.fromDTO(entity.getMediaItem(),
               em));
      }
      if (this.category != null) {
         entity.setCategory(this.category.fromDTO(entity.getCategory(), em));
      }
      entity.setDescription(this.description);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public NestedMediaItemDto getMediaItem() {
      return this.mediaItem;
   }

   public void setMediaItem(final NestedMediaItemDto mediaItem) {
      this.mediaItem = mediaItem;
   }

   public NestedFlightCategoryDto getCategory() {
      return this.category;
   }

   public void setCategory(final NestedFlightCategoryDto category) {
      this.category = category;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }
   
}
