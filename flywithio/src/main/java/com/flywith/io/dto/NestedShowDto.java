package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.Show;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class NestedShowDto implements Serializable {

   private Long id;
   private String displayTitle;

   public NestedShowDto() {
	   
   }

   public NestedShowDto(final Show entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.displayTitle = entity.toString();
      }
   }

   public Show fromDTO(Show entity, EntityManager em) {
      if (entity == null) {
         entity = new Show();
      }
      if (this.id != null) {
         TypedQuery<Show> findByIdQuery = em.createQuery(
               "SELECT DISTINCT s FROM Show s WHERE s.id = :entityId",
               Show.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getDisplayTitle() {
      return this.displayTitle;
   }
   
}
