package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.xml.bind.annotation.XmlRootElement;

import com.flywith.io.model.TicketCategory;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class TicketCategoryDto implements Serializable {

   private Long id;
   private String description;

   public TicketCategoryDto() {
	   
   }

   public TicketCategoryDto(final TicketCategory entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.description = entity.getDescription();
      }
   }

   public TicketCategory fromDTO(TicketCategory entity, EntityManager em) {
      if (entity == null) {
         entity = new TicketCategory();
      }
      entity.setDescription(this.description);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }
   
}
