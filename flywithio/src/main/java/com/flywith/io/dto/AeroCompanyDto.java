package com.flywith.io.dto;

import javax.persistence.EntityManager;

import com.flywith.io.model.AeroCompany;

public class AeroCompanyDto {

	private Long id;
	private String name;
	private String link;
	
	public AeroCompanyDto() {
		
	}

	public AeroCompanyDto(AeroCompany entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.link = entity.getLink();
	}
	
	public AeroCompany fromDto(AeroCompany entity, EntityManager em) {
		if(entity == null) {
			entity = new AeroCompany();
		}
		entity.setName(this.name);
		entity.setLink(this.link);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setLink(final String link) {
		this.link = link;
	}
	
}
