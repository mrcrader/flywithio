package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.Airport;

public class NestedAirportDto implements Serializable {
	private static final long serialVersionUID = 1048516676729002063L;
	
	   private Long id;
	   private String name;
	   private IataCodeDto iataCode;
	   private AddressDto address;

	   public NestedAirportDto() {
		   
	   }

	   public NestedAirportDto(final Airport entity) {
	      if (entity != null) {
	         this.id = entity.getId();
	         this.name = entity.getName();
	         this.address = new AddressDto(entity.getAddress());
	         this.iataCode = new IataCodeDto(entity.getIataCode());
	      }
	   }

	   public Airport fromDto(Airport entity, EntityManager em)
	   {
	      if (entity == null) {
	         entity = new Airport();
	      }
	      if (this.id != null) {
	         TypedQuery<Airport> findByIdQuery = em.createQuery(
	               "SELECT DISTINCT a FROM Airport a WHERE a.id = :entityId",
	               Airport.class);
	         findByIdQuery.setParameter("entityId", this.id);
	         try {
	            entity = findByIdQuery.getSingleResult();
	         }
	         catch (javax.persistence.NoResultException nre) {
	            entity = null;
	         }
	         return entity;
	      }
	      
	      entity.setName(this.name);
	      if (this.address != null) {
	         entity.setAddress(this.address.fromDTO(entity.getAddress(), em));
	      } 
	      if (this.iataCode != null){
	          entity.setIataCode(this.iataCode.fromDto(entity.getIataCode(), em));
	      }
	      entity = em.merge(entity);
	      return entity;
	   }

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public IataCodeDto getIataCode() {
		return iataCode;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIataCode(IataCodeDto iataCode) {
		this.iataCode = iataCode;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}
	
}
