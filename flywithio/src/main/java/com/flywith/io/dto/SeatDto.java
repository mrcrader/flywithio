package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedSectionDto;
import com.flywith.io.model.Seat;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class SeatDto implements Serializable {

   private NestedSectionDto section;
   private int rowNumber;
   private int number;

   public SeatDto() {
	   
   }

   public SeatDto(final Seat entity) {
      if (entity != null) {
         this.section = new NestedSectionDto(entity.getSection());
         this.rowNumber = entity.getRowNumber();
         this.number = entity.getNumber();
      }
   }

   public Seat fromDTO(Seat entity, EntityManager em) {
      if (entity == null) {
         entity = new Seat();
      }
      return entity;
   }

   public NestedSectionDto getSection() {
      return this.section;
   }

   public void setSection(final NestedSectionDto section) {
      this.section = section;
   }

   public int getRowNumber() {
      return this.rowNumber;
   }

   public void setRowNumber(final int rowNumber) {
      this.rowNumber = rowNumber;
   }

   public int getNumber() {
      return this.number;
   }

   public void setNumber(final int number) {
      this.number = number;
   }
   
}
