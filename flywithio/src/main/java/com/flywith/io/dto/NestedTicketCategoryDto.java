package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.TicketCategory;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class NestedTicketCategoryDto implements Serializable {

   private Long id;
   private String description;

   public NestedTicketCategoryDto() {
	   
   }

   public NestedTicketCategoryDto(final TicketCategory entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.description = entity.getDescription();
      }
   }

   public TicketCategory fromDTO(TicketCategory entity, EntityManager em) {
      if (entity == null) {
         entity = new TicketCategory();
      }
      if (this.id != null) {
         TypedQuery<TicketCategory> findByIdQuery = em
               .createQuery(
                     "SELECT DISTINCT t FROM TicketCategory t WHERE t.id = :entityId",
                     TicketCategory.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity.setDescription(this.description);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }
   
}
