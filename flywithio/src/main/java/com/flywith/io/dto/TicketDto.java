package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedTicketCategoryDto;
import com.flywith.io.dto.SeatDto;
import com.flywith.io.model.Ticket;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class TicketDto implements Serializable {

   private Long id;
   private NestedTicketCategoryDto ticketCategory;
   private float price;
   private SeatDto seat;

   public TicketDto() {
	   
   }

   public TicketDto(final Ticket entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.ticketCategory = new NestedTicketCategoryDto(
               entity.getTicketCategory());
         this.price = entity.getPrice();
         this.seat = new SeatDto(entity.getSeat());
      }
   }

   public Ticket fromDTO(Ticket entity, EntityManager em) {
      if (entity == null) {
         entity = new Ticket();
      }
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public NestedTicketCategoryDto getTicketCategory() {
      return this.ticketCategory;
   }

   public void setTicketCategory(final NestedTicketCategoryDto ticketCategory) {
      this.ticketCategory = ticketCategory;
   }

   public float getPrice() {
      return this.price;
   }

   public void setPrice(final float price) {
      this.price = price;
   }

   public SeatDto getSeat() {
      return this.seat;
   }

   public void setSeat(final SeatDto seat) {
      this.seat = seat;
   }
   
}
