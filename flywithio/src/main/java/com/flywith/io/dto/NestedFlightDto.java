package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.Flight;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class NestedFlightDto implements Serializable {

   private Long id;
   private String name;
   private String description;

   public NestedFlightDto() {
	   
   }

   public NestedFlightDto(final Flight entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.name = entity.getName();
         this.description = entity.getDescription();
      }
   }

   public Flight fromDTO(Flight entity, EntityManager em) {
      if (entity == null) {
         entity = new Flight();
      }
      if (this.id != null) {
         TypedQuery<Flight> findByIdQuery = em.createQuery(
               "SELECT DISTINCT f FROM Flight f WHERE f.id = :entityId",
               Flight.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity.setName(this.name);
      entity.setDescription(this.description);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }
   
}
