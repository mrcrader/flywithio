package com.flywith.io.dto;

import javax.persistence.EntityManager;

import com.flywith.io.model.IcaoCode;

public class IcaoCodeDto {

	private Long id;
	private String code;
	
	public IcaoCodeDto() {
		
	}
	
	public IcaoCodeDto(IcaoCode entity) {
		this.id = entity.getId();
		this.code = entity.getCode();
	}
	
	public IcaoCode fromDto(IcaoCode entity, EntityManager em) {
		if(entity == null) {
			entity = new IcaoCode();
		}
		entity.setCode(this.code);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
