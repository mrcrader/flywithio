package com.flywith.io.dto;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.Venue;

/**
 * 
 * @author IBA Group
 * @since 2018
 * 
 */

@SuppressWarnings("serial")
public class NestedVenueDto implements Serializable {

   private Long id;
   private String name;
   private AeroCompanyDto aerocompany;
   private String description;
   private int capacity;

   public NestedVenueDto() {
	   
   }

   public NestedVenueDto(final Venue entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.name = entity.getName();
         this.aerocompany = new AeroCompanyDto(entity.getAeroCompany());
         this.description = entity.getDescription();
         this.capacity = entity.getCapacity();
      }
   }

   public Venue fromDto(Venue entity, EntityManager em) {
      if (entity == null) {
         entity = new Venue();
      }
      if (this.id != null) {
         TypedQuery<Venue> findByIdQuery = em.createQuery(
               "SELECT DISTINCT v FROM Venue v WHERE v.id = :entityId",
               Venue.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity.setName(this.name);
      if (this.aerocompany != null) {
         entity.setAeroCompany(this.aerocompany.fromDto(entity.getAeroCompany(), em));
      }
      entity.setDescription(this.description);
      entity.setCapacity(this.capacity);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public AeroCompanyDto getAeroCompany() {
      return this.aerocompany;
   }

   public void setAeroCompany(final AeroCompanyDto aerocompany) {
      this.aerocompany = aerocompany;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }

   public int getCapacity() {
      return this.capacity;
   }

   public void setCapacity(final int capacity) {
      this.capacity = capacity;
   }
   
}