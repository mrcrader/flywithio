package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedPerformanceDto;
import com.flywith.io.dto.NestedSectionDto;
import com.flywith.io.model.SectionAllocation;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class SectionAllocationDto implements Serializable {

   private int occupiedCount;
   private NestedPerformanceDto performance;
   private NestedSectionDto section;
   private Long id;

   public SectionAllocationDto() {
	   
   }

   public SectionAllocationDto(final SectionAllocation entity) {
      if (entity != null) {
         this.occupiedCount = entity.getOccupiedCount();
         this.performance = new NestedPerformanceDto(entity.getPerformance());
         this.section = new NestedSectionDto(entity.getSection());
         this.id = entity.getId();
      }
   }

   public SectionAllocation fromDTO(SectionAllocation entity, EntityManager em) {
      if (entity == null) {
         entity = new SectionAllocation();
      }
      entity = em.merge(entity);
      return entity;
   }

   public int getOccupiedCount() {
      return this.occupiedCount;
   }

   public void setOccupiedCount(final int occupiedCount) {
      this.occupiedCount = occupiedCount;
   }

   public NestedPerformanceDto getPerformance() {
      return this.performance;
   }

   public void setPerformance(final NestedPerformanceDto performance) {
      this.performance = performance;
   }

   public NestedSectionDto getSection() {
      return this.section;
   }

   public void setSection(final NestedSectionDto section) {
      this.section = section;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }
   
}
