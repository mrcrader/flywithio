package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedSectionDto;
import com.flywith.io.dto.NestedShowDto;
import com.flywith.io.dto.NestedTicketCategoryDto;
import com.flywith.io.model.TicketPrice;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@SuppressWarnings("serial")
public class TicketPriceDto implements Serializable {

   private Long id;
   private NestedShowDto show;
   private NestedSectionDto section;
   private NestedTicketCategoryDto ticketCategory;
   private float price;
   private String displayTitle;

   public TicketPriceDto() {
	   
   }

   public TicketPriceDto(final TicketPrice entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.show = new NestedShowDto(entity.getShow());
         this.section = new NestedSectionDto(entity.getSection());
         this.ticketCategory = new NestedTicketCategoryDto(
               entity.getTicketCategory());
         this.price = entity.getPrice();
         this.displayTitle = entity.toString();
      }
   }

   public TicketPrice fromDTO(TicketPrice entity, EntityManager em) {
      if (entity == null) {
         entity = new TicketPrice();
      }
      if (this.show != null) {
         entity.setShow(this.show.fromDTO(entity.getShow(), em));
      }
      if (this.section != null) {
         entity.setSection(this.section.fromDTO(entity.getSection(), em));
      }
      if (this.ticketCategory != null) {
         entity.setTicketCategory(this.ticketCategory.fromDTO(
               entity.getTicketCategory(), em));
      }
      entity.setPrice(this.price);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public NestedShowDto getShow() {
      return this.show;
   }

   public void setShow(final NestedShowDto show) {
      this.show = show;
   }

   public NestedSectionDto getSection() {
      return this.section;
   }

   public void setSection(final NestedSectionDto section) {
      this.section = section;
   }

   public NestedTicketCategoryDto getTicketCategory() {
      return this.ticketCategory;
   }

   public void setTicketCategory(final NestedTicketCategoryDto ticketCategory) {
      this.ticketCategory = ticketCategory;
   }

   public float getPrice() {
      return this.price;
   }

   public void setPrice(final float price) {
      this.price = price;
   }

   public String getDisplayTitle() {
      return this.displayTitle;
   }
   
}
