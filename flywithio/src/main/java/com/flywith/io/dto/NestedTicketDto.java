package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.dto.SeatDto;
import com.flywith.io.model.Ticket;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class NestedTicketDto implements Serializable {

   private Long id;
   private float price;
   private SeatDto seat;

   public NestedTicketDto() {
	   
   }

   public NestedTicketDto(final Ticket entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.price = entity.getPrice();
         this.seat = new SeatDto(entity.getSeat());
      }
   }

   public Ticket fromDTO(Ticket entity, EntityManager em) {
      if (entity == null) {
         entity = new Ticket();
      }
      if (this.id != null) {
         TypedQuery<Ticket> findByIdQuery = em.createQuery(
               "SELECT DISTINCT t FROM Ticket t WHERE t.id = :entityId",
               Ticket.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public float getPrice() {
      return this.price;
   }

   public void setPrice(final float price) {
      this.price = price;
   }

   public SeatDto getSeat() {
      return this.seat;
   }

   public void setSeat(final SeatDto seat) {
      this.seat = seat;
   }
   
}
