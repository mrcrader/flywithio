package com.flywith.io.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.Airplane;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

public class NestedAirplaneDto implements Serializable {

   private static final long serialVersionUID = -7297105975856445887L;
   
   private Long id;
   private String model;
   private IcaoCodeDto icaoCode;
   private AeroCompanyDto aeroCompany;
   private NestedMediaItemDto mediaItem;
   private String description;
   private Set<NestedSectionDto> sections = new HashSet<NestedSectionDto>();
   private int capacity;

   public NestedAirplaneDto() {
	   
   }

   public NestedAirplaneDto(final Airplane entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.model = entity.getModel();
         this.icaoCode = new IcaoCodeDto(entity.getIcaoCode());
         this.aeroCompany = new AeroCompanyDto(entity.getAeroCompany());
         this.description = entity.getDescription();
         this.capacity = entity.getCapacity();
      }
   }

   public Airplane fromDto(Airplane entity, EntityManager em) {
      if (entity == null) {
         entity = new Airplane();
      }
      if (this.id != null) {
         TypedQuery<Airplane> findByIdQuery = em.createQuery(
               "SELECT DISTINCT a FROM Airplane a WHERE a.id = :entityId",
               Airplane.class);
         findByIdQuery.setParameter("entityId", this.id);
         try {
            entity = findByIdQuery.getSingleResult();
         }
         catch (javax.persistence.NoResultException nre) {
            entity = null;
         }
         return entity;
      }
      entity.setModel(this.model);
      if (this.icaoCode != null) {
         entity.setIcaoCode(this.icaoCode.fromDto(entity.getIcaoCode(), em));
      }
      if (this.aeroCompany != null) {
         entity.setAeroCompany(this.aeroCompany.fromDto(entity.getAeroCompany(), em));
      }
      entity.setDescription(this.description);
      entity.setCapacity(this.capacity);
      entity = em.merge(entity);
      return entity;
   }

	public Long getId() {
		return id;
	}
	
	public String getModel() {
		return model;
	}
	
	public IcaoCodeDto getIcaoCode() {
		return icaoCode;
	}
	
	public AeroCompanyDto getAeroCompany() {
		return aeroCompany;
	}
	
	public NestedMediaItemDto getMediaItem() {
		return mediaItem;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Set<NestedSectionDto> getSections() {
		return sections;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public void setIcaoCode(IcaoCodeDto icaoCode) {
		this.icaoCode = icaoCode;
	}
	
	public void setAeroCompany(AeroCompanyDto aeroCompany) {
		this.aeroCompany = aeroCompany;
	}
	
	public void setMediaItem(NestedMediaItemDto mediaItem) {
		this.mediaItem = mediaItem;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setSections(Set<NestedSectionDto> sections) {
		this.sections = sections;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
   
}
