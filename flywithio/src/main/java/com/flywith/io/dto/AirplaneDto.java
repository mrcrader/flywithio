package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import com.flywith.io.dto.AddressDto;
import com.flywith.io.dto.NestedMediaItemDto;
import com.flywith.io.dto.NestedSectionDto;
import com.flywith.io.model.Section;
import com.flywith.io.model.SectionAllocation;
import com.flywith.io.model.Show;
import com.flywith.io.model.TicketPrice;
import com.flywith.io.model.Airplane;

import java.util.Iterator;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
public class AirplaneDto implements Serializable {

   private static final long serialVersionUID = 8809952498978516387L;
	
   private Long id;
   private String model;
   private IcaoCodeDto icaoCode;
   private AeroCompanyDto aeroCompany;
   private NestedMediaItemDto mediaItem;
   private String description;
   private Set<NestedSectionDto> sections = new HashSet<NestedSectionDto>();
   private int capacity;

   public AirplaneDto() {
	   
   }

   public AirplaneDto(final Airplane entity) {
      if (entity != null){
         this.id = entity.getId();
         this.model = entity.getModel();
         this.icaoCode = new IcaoCodeDto(entity.getIcaoCode());
         this.aeroCompany = new AeroCompanyDto(entity.getAeroCompany());
         this.mediaItem = new NestedMediaItemDto(entity.getMediaItem());
         this.description = entity.getDescription();
         Iterator<Section> iterSections = entity.getSections().iterator();
         while (iterSections.hasNext()) {
            Section element = iterSections.next();
            this.sections.add(new NestedSectionDto(element));
         }
         this.capacity = entity.getCapacity();
      }
   }

   public Airplane fromDto(Airplane entity, EntityManager em) {
      if (entity == null) {
         entity = new Airplane();
      }
      entity.setModel(this.model);
      if (this.icaoCode != null) {
    	  entity.setIcaoCode(this.icaoCode.fromDto(entity.getIcaoCode(),
    			  em));
      }
      if (this.aeroCompany != null) {
    	  entity.setAeroCompany(this.aeroCompany.fromDto(entity.getAeroCompany(),
    			  em));
      }
      if (this.mediaItem != null) {
         entity.setMediaItem(this.mediaItem.fromDTO(entity.getMediaItem(),
               em));
      }
      entity.setDescription(this.description);
      Iterator<Section> iterSections = entity.getSections().iterator();
      while (iterSections.hasNext()) {
         boolean found = false;
         Section section = iterSections.next();
         Iterator<NestedSectionDto> iterDtoSections = this.getSections()
               .iterator();
         while (iterDtoSections.hasNext()) {
            NestedSectionDto dtoSection = iterDtoSections.next();
            if (dtoSection.getId().equals(section.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            iterSections.remove();
            List<SectionAllocation> sectionAllocations = findSectionAllocationBySection(section, em);
	        for(SectionAllocation sectionAllocation: sectionAllocations) {
               em.remove(sectionAllocation);
            }
            List<TicketPrice> ticketPrices = findTicketPricesBySection(section, em);
            for(TicketPrice ticketPrice: ticketPrices) {
               Show show = ticketPrice.getShow();
               show.getTicketPrices().remove(ticketPrice);
               em.remove(ticketPrice);
            }
            em.remove(section);
         }
      }
      Iterator<NestedSectionDto> iterDtoSections = this.getSections()
            .iterator();
      while (iterDtoSections.hasNext()) {
         boolean found = false;
         NestedSectionDto dtoSection = iterDtoSections.next();
         iterSections = entity.getSections().iterator();
         while (iterSections.hasNext()){
            Section section = iterSections.next();
            if (dtoSection.getId().equals(section.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            Iterator<Section> resultIter = em
                  .createQuery("SELECT DISTINCT s FROM Section s",
                        Section.class).getResultList().iterator();
            while (resultIter.hasNext()) {
               Section result = resultIter.next();
               if (result.getId().equals(dtoSection.getId())) {
                  entity.getSections().add(result);
                  break;
               }
            }
         }
      }
      entity.setCapacity(this.capacity);
      entity = em.merge(entity);
      return entity;
   }

   public List<SectionAllocation> findSectionAllocationBySection(Section section, EntityManager em) {
      CriteriaQuery<SectionAllocation> criteria = em
             .getCriteriaBuilder().createQuery(SectionAllocation.class);
      Root<SectionAllocation> from = criteria.from(SectionAllocation.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate sectionIsSame = builder.equal(from.get("section"), section);
      return em.createQuery(
             criteria.select(from).where(sectionIsSame)).getResultList();
   }
   
   public List<TicketPrice> findTicketPricesBySection(Section section, EntityManager em) {
      CriteriaQuery<TicketPrice> criteria = em
             .getCriteriaBuilder().createQuery(TicketPrice.class);
      Root<TicketPrice> from = criteria.from(TicketPrice.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate sectionIsSame = builder.equal(from.get("section"), section);
      return em.createQuery(
             criteria.select(from).where(sectionIsSame)).getResultList();
   }

	public Long getId() {
		return id;
	}
	
	public String getModel() {
		return model;
	}
	
	public IcaoCodeDto getIcaoCode() {
		return icaoCode;
	}
	
	public AeroCompanyDto getAeroCompany() {
		return aeroCompany;
	}
	
	public NestedMediaItemDto getMediaItem() {
		return mediaItem;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Set<NestedSectionDto> getSections() {
		return sections;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public void setIcaoCode(IcaoCodeDto icaoCode) {
		this.icaoCode = icaoCode;
	}
	
	public void setAeroCompany(AeroCompanyDto aeroCompany) {
		this.aeroCompany = aeroCompany;
	}
	
	public void setMediaItem(NestedMediaItemDto mediaItem) {
		this.mediaItem = mediaItem;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setSections(Set<NestedSectionDto> sections) {
		this.sections = sections;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
   
}
