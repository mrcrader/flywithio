package com.flywith.io.dto;

import java.util.Date;

import javax.persistence.EntityManager;

import com.flywith.io.model.User;

public class UserDto {

	private Long id;
	private String username;
	private String name;
	private String surname;
	private String email;
	private Date dateOfBirth;
	private AddressDto address;
	
	public UserDto() {
		
	}
	
	public UserDto(final User entity) {
		if(entity != null) {
			this.id = entity.getId();
			this.name = entity.getName();
			this.dateOfBirth = entity.getDateOfBirth();
			this.surname = entity.getSurname();
			this.username = entity.getUsername();
			this.address = new AddressDto(entity.getAddress());
		}
	}
	
	public User fromDto(User entity, EntityManager em) {
		if(entity == null) {
			entity = new User();
		}
		if(this.address != null) {
			entity.setAddress(( this.address).fromDTO(entity.getAddress(),
					em));
		}
		entity.setDateOfBirth(this.dateOfBirth);
		entity.setName(this.name);
		entity.setEmail(this.email);
		entity.setSurname(this.surname);
		entity.setUsername(this.username);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getEmail() {
		return email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setDateOfBirth(final Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setAddress(final AddressDto address) {
		this.address = address;
	}
	
}
