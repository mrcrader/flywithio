package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.xml.bind.annotation.XmlRootElement;

import com.flywith.io.model.FlightCategory;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class FlightCategoryDto implements Serializable {

   private Long id;
   private String description;

   public FlightCategoryDto() {
	   
   }

   public FlightCategoryDto(final FlightCategory entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.description = entity.getDescription();
      }
   }

   public FlightCategory fromDTO(FlightCategory entity, EntityManager em) {
      if (entity == null) {
         entity = new FlightCategory();
      }
      entity.setDescription(this.description);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }
   
}
