package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import com.flywith.io.dto.NestedFlightDto;
import com.flywith.io.dto.NestedPerformanceDto;
import com.flywith.io.dto.NestedTicketPriceDto;
import com.flywith.io.model.Booking;
import com.flywith.io.model.Performance;
import com.flywith.io.model.SectionAllocation;
import com.flywith.io.model.Show;
import com.flywith.io.model.TicketPrice;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 * 
 */

@XmlRootElement
@SuppressWarnings("serial")
public class ShowDto implements Serializable {

   private Long id;
   private NestedFlightDto flight;
   private Set<NestedPerformanceDto> performances = new HashSet<NestedPerformanceDto>();
   private NestedVenueDto venue;
   private Set<NestedTicketPriceDto> ticketPrices = new HashSet<NestedTicketPriceDto>();
   private String displayTitle;

   public ShowDto() {
	   
   }

   public ShowDto(final Show entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.flight = new NestedFlightDto(entity.getFlight());
         Iterator<Performance> iterPerformances = entity.getPerformances()
               .iterator();
         while (iterPerformances.hasNext()) {
            Performance element = iterPerformances.next();
            this.performances.add(new NestedPerformanceDto(element));
         }
         this.venue = new NestedVenueDto(entity.getVenue());
         Iterator<TicketPrice> iterTicketPrices = entity.getTicketPrices()
               .iterator();
         while (iterTicketPrices.hasNext()) {
            TicketPrice element = iterTicketPrices.next();
            this.ticketPrices.add(new NestedTicketPriceDto(element));
         }
         this.displayTitle = entity.toString();
      }
   }

   public Show fromDTO(Show entity, EntityManager em) {
      if (entity == null) {
         entity = new Show();
      }
      if (this.flight != null) {
         entity.setFlight(this.flight.fromDTO(entity.getFlight(), em));
      }
      Iterator<Performance> iterPerformances = entity.getPerformances()
            .iterator();
      while (iterPerformances.hasNext()) {
         boolean found = false;
         Performance performance = iterPerformances.next();
         Iterator<NestedPerformanceDto> iterDtoPerformances = this
               .getPerformances().iterator();
         while (iterDtoPerformances.hasNext()) {
            NestedPerformanceDto dtoPerformance = iterDtoPerformances
                  .next();
            if (dtoPerformance.getId().equals(performance.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            iterPerformances.remove();
            List<SectionAllocation> sectionAllocations = findSectionAllocationsByPerformance(performance, em);
            for(SectionAllocation sectionAllocation: sectionAllocations) {
               em.remove(sectionAllocation);
            }
            List<Booking> bookings = findBookingsByPerformance(performance, em);
            for(Booking booking: bookings) {
               em.remove(booking);
            }
            em.remove(performance);
         }
      }
      Iterator<NestedPerformanceDto> iterDtoPerformances = this
            .getPerformances().iterator();
      while (iterDtoPerformances.hasNext()) {
         boolean found = false;
         NestedPerformanceDto dtoPerformance = iterDtoPerformances.next();
         iterPerformances = entity.getPerformances().iterator();
         while (iterPerformances.hasNext()) {
            Performance performance = iterPerformances.next();
            if (dtoPerformance.getId().equals(performance.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            Iterator<Performance> resultIter = em
                  .createQuery("SELECT DISTINCT p FROM Performance p",
                        Performance.class).getResultList().iterator();
            while (resultIter.hasNext()) {
               Performance result = resultIter.next();
               if (result.getId().equals(dtoPerformance.getId())) {
                  entity.getPerformances().add(result);
                  break;
               }
            }
         }
      }
      if (this.venue != null) {
         entity.setVenue(this.venue.fromDto(entity.getVenue(), em));
      }
      Iterator<TicketPrice> iterTicketPrices = entity.getTicketPrices()
            .iterator();
      while (iterTicketPrices.hasNext()) {
         boolean found = false;
         TicketPrice ticketPrice = iterTicketPrices.next();
         Iterator<NestedTicketPriceDto> iterDtoTicketPrices = this
               .getTicketPrices().iterator();
         while (iterDtoTicketPrices.hasNext()) {
            NestedTicketPriceDto dtoTicketPrice = iterDtoTicketPrices
                  .next();
            if (dtoTicketPrice.getId().equals(ticketPrice.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            iterTicketPrices.remove();
         }
      }
      Iterator<NestedTicketPriceDto> iterDtoTicketPrices = this
            .getTicketPrices().iterator();
      while (iterDtoTicketPrices.hasNext()) {
         boolean found = false;
         NestedTicketPriceDto dtoTicketPrice = iterDtoTicketPrices.next();
         iterTicketPrices = entity.getTicketPrices().iterator();
         while (iterTicketPrices.hasNext()) {
            TicketPrice ticketPrice = iterTicketPrices.next();
            if (dtoTicketPrice.getId().equals(ticketPrice.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            Iterator<TicketPrice> resultIter = em
                  .createQuery("SELECT DISTINCT t FROM TicketPrice t",
                        TicketPrice.class).getResultList().iterator();
            while (resultIter.hasNext()) {
               TicketPrice result = resultIter.next();
               if (result.getId().equals(dtoTicketPrice.getId())) {
                  entity.getTicketPrices().add(result);
                  break;
               }
            }
         }
      }
      entity = em.merge(entity);
      return entity;
   }

   public List<SectionAllocation> findSectionAllocationsByPerformance(Performance performance, EntityManager em) {
      CriteriaQuery<SectionAllocation> criteria = em
             .getCriteriaBuilder().createQuery(SectionAllocation.class);
      Root<SectionAllocation> from = criteria.from(SectionAllocation.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate performanceIsSame = builder.equal(from.get("performance"), performance);
      return em.createQuery(
             criteria.select(from).where(performanceIsSame)).getResultList();
   }
   
   public List<Booking> findBookingsByPerformance(Performance performance, EntityManager em) {
      CriteriaQuery<Booking> criteria = em.getCriteriaBuilder().createQuery(Booking.class);
      Root<Booking> from = criteria.from(Booking.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate performanceIsSame = builder.equal(from.get("performance"), performance);
      return em.createQuery(
             criteria.select(from).where(performanceIsSame)).getResultList();
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public NestedFlightDto getflight() {
      return this.flight;
   }

   public void setflight(final NestedFlightDto flight) {
      this.flight = flight;
   }

   public Set<NestedPerformanceDto> getPerformances() {
      return this.performances;
   }

   public void setPerformances(final Set<NestedPerformanceDto> performances) {
      this.performances = performances;
   }

   public NestedVenueDto getVenue() {
      return this.venue;
   }

   public void setVenue(final NestedVenueDto venue) {
      this.venue = venue;
   }

   public Set<NestedTicketPriceDto> getTicketPrices() {
      return this.ticketPrices;
   }

   public void setTicketPrices(final Set<NestedTicketPriceDto> ticketPrices) {
      this.ticketPrices = ticketPrices;
   }

   public String getDisplayTitle() {
      return this.displayTitle;
   }
   
}
