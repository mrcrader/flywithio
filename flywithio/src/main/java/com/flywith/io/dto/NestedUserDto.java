package com.flywith.io.dto;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.flywith.io.model.User;

public class NestedUserDto {

	private Long id;
	private String username;
	private String name;
	private String surname;
	private String email;
	private AddressDto address = new AddressDto();
	private Date dateOfBirth;
	private UserPasswordsDto userPassword = new UserPasswordsDto();

	   public NestedUserDto() {
		   
	   }

	   public NestedUserDto(final User entity) {
	      if (entity != null) {
	         this.id = entity.getId();
	         this.name = entity.getName();
	         this.username = entity.getUsername();
	         this.surname = entity.getSurname();
	         this.email = entity.getEmail();
	         this.dateOfBirth = entity.getDateOfBirth();
	         this.address = new AddressDto(entity.getAddress());
	         this.userPassword = new UserPasswordsDto(entity.getUserPassword());
	      }
	   }

	   public User fromDto(User entity, EntityManager em)
	   {
	      if (entity == null) {
	         entity = new User();
	      }
	      if (this.id != null) {
	         TypedQuery<User> findByIdQuery = em.createQuery(
	               "SELECT DISTINCT u FROM User u WHERE u.id = :entityId",
	               User.class);
	         findByIdQuery.setParameter("entityId", this.id);
	         try {
	            entity = findByIdQuery.getSingleResult();
	         }
	         catch (javax.persistence.NoResultException nre) {
	            entity = null;
	         }
	         return entity;
	      }
	      
	      entity.setName(this.name);
	      entity.setEmail(this.email);
	      entity.setSurname(this.surname);
	      entity.setUsername(this.username);
	      entity.setDateOfBirth(this.dateOfBirth);
	      if (this.address != null) {
	         entity.setAddress(this.address.fromDTO(entity.getAddress(), em));
	      } 
	      if (this.userPassword != null){
	          entity.setUserPassword(this.userPassword.fromDto(entity.getUserPassword(), em));
	      }
	      entity = em.merge(entity);
	      return entity;
	   }

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getEmail() {
		return email;
	}

	public AddressDto getAddress() {
		return address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public UserPasswordsDto getUserPassword() {
		return userPassword;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setUserPassword(UserPasswordsDto userPassword) {
		this.userPassword = userPassword;
	}
	
}
