package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import java.util.Iterator;

import javax.xml.bind.annotation.XmlRootElement;

import com.flywith.io.model.Section;
import com.flywith.io.model.SectionAllocation;
import com.flywith.io.model.Show;
import com.flywith.io.model.TicketPrice;
import com.flywith.io.model.Venue;

/**
 * 
 * @author IBA Group
 * @since 2018
 * 
 */

@XmlRootElement
@SuppressWarnings("serial")
public class VenueDto implements Serializable {

   private Long id;
   private String name;
   private NestedAeroCompanyDto aerocompany;
   private NestedMediaItemDto mediaItem;
   private String description;
   private Set<NestedSectionDto> sections = new HashSet<NestedSectionDto>();
   private int capacity;

   public VenueDto() {
	   
   }

   public VenueDto(final Venue entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.name = entity.getName();
         this.aerocompany = new NestedAeroCompanyDto(entity.getAeroCompany());
         this.mediaItem = new NestedMediaItemDto(entity.getMediaItem());
         this.description = entity.getDescription();
         Iterator<Section> iterSections = entity.getSections().iterator();
         while (iterSections.hasNext()) {
            Section element = iterSections.next();
            this.sections.add(new NestedSectionDto(element));
         }
         this.capacity = entity.getCapacity();
      }
   }

   public Venue fromDto(Venue entity, EntityManager em) {
      if (entity == null) {
         entity = new Venue();
      }
      entity.setName(this.name);
      if (this.aerocompany != null) {
         entity.setAeroCompany(this.aerocompany.fromDto(entity.getAeroCompany(), em));
      }
      if (this.mediaItem != null) {
         entity.setMediaItem(this.mediaItem.fromDTO(entity.getMediaItem(),
               em));
      }
      entity.setDescription(this.description);
      Iterator<Section> iterSections = entity.getSections().iterator();
      while (iterSections.hasNext()) {
         boolean found = false;
         Section section = iterSections.next();
         Iterator<NestedSectionDto> iterDtoSections = this.getSections()
               .iterator();
         while (iterDtoSections.hasNext()) {
            NestedSectionDto DtoSection = iterDtoSections.next();
            if (DtoSection.getId().equals(section.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            iterSections.remove();
            List<SectionAllocation> sectionAllocations = findSectionAllocationBySection(section, em);
	        for(SectionAllocation sectionAllocation: sectionAllocations) {
               em.remove(sectionAllocation);
            }
            List<TicketPrice> ticketPrices = findTicketPricesBySection(section, em);
            for(TicketPrice ticketPrice: ticketPrices) {
               Show show = ticketPrice.getShow();
               show.getTicketPrices().remove(ticketPrice);
               em.remove(ticketPrice);
            }
            em.remove(section);
         }
      }
      Iterator<NestedSectionDto> iterDtoSections = this.getSections()
            .iterator();
      while (iterDtoSections.hasNext()) {
         boolean found = false;
         NestedSectionDto DtoSection = iterDtoSections.next();
         iterSections = entity.getSections().iterator();
         while (iterSections.hasNext()) {
            Section section = iterSections.next();
            if (DtoSection.getId().equals(section.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            Iterator<Section> resultIter = em
                  .createQuery("SELECT DISTINCT s FROM Section s",
                        Section.class).getResultList().iterator();
            while (resultIter.hasNext()) {
               Section result = resultIter.next();
               if (result.getId().equals(DtoSection.getId())) {
                  entity.getSections().add(result);
                  break;
               }
            }
         }
      }
      entity.setCapacity(this.capacity);
      entity = em.merge(entity);
      return entity;
   }

   public List<SectionAllocation> findSectionAllocationBySection(Section section, EntityManager em) {
      CriteriaQuery<SectionAllocation> criteria = em
             .getCriteriaBuilder().createQuery(SectionAllocation.class);
      Root<SectionAllocation> from = criteria.from(SectionAllocation.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate sectionIsSame = builder.equal(from.get("section"), section);
      return em.createQuery(
             criteria.select(from).where(sectionIsSame)).getResultList();
   }
   
   public List<TicketPrice> findTicketPricesBySection(Section section, EntityManager em) {
      CriteriaQuery<TicketPrice> criteria = em
             .getCriteriaBuilder().createQuery(TicketPrice.class);
      Root<TicketPrice> from = criteria.from(TicketPrice.class);
      CriteriaBuilder builder = em.getCriteriaBuilder();
      Predicate sectionIsSame = builder.equal(from.get("section"), section);
      return em.createQuery(
             criteria.select(from).where(sectionIsSame)).getResultList();
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public NestedAeroCompanyDto getAeroCompany() {
      return this.aerocompany;
   }

   public void setAeroCompany(final NestedAeroCompanyDto aerocompany) {
      this.aerocompany = aerocompany;
   }

   public NestedMediaItemDto getMediaItem() {
      return this.mediaItem;
   }

   public void setMediaItem(final NestedMediaItemDto mediaItem) {
      this.mediaItem = mediaItem;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }

   public Set<NestedSectionDto> getSections() {
      return this.sections;
   }

   public void setSections(final Set<NestedSectionDto> sections) {
      this.sections = sections;
   }

   public int getCapacity() {
      return this.capacity;
   }

   public void setCapacity(final int capacity) {
      this.capacity = capacity;
   }
   
}
