package com.flywith.io.dto;

import javax.persistence.EntityManager;

import com.flywith.io.model.IataCode;

public class IataCodeDto {

	private Long id;
	private String code;
	
	public IataCodeDto() {
		
	}
	
	public IataCodeDto(IataCode entity) {
		this.id = entity.getId();
		this.code = entity.getCode();
	}
	
	public IataCode fromDto(IataCode entity, EntityManager em) {
		if(entity == null) {
			entity = new IataCode();
		}
		entity.setCode(this.code);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
