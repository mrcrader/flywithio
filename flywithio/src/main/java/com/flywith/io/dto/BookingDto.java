package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;
import java.util.Set;
import java.util.HashSet;

import com.flywith.io.dto.NestedPerformanceDto;
import com.flywith.io.dto.NestedTicketDto;
import com.flywith.io.model.Booking;
import com.flywith.io.model.Ticket;

import java.util.Iterator;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@XmlRootElement
@SuppressWarnings("serial")
public class BookingDto implements Serializable {

   private float totalTicketPrice;
   private Long id;
   private Set<NestedTicketDto> tickets = new HashSet<NestedTicketDto>();
   private Date createdOn;
   private String cancellationCode;
   private String contactEmail;
   private NestedPerformanceDto performance;

   public BookingDto() {
	   
   }

   public BookingDto(final Booking entity) {
      if (entity != null) {
         this.totalTicketPrice = entity.getTotalTicketPrice();
         this.id = entity.getId();
         Iterator<Ticket> iterTickets = entity.getTickets().iterator();
         while (iterTickets.hasNext()) {
            Ticket element = iterTickets.next();
            this.tickets.add(new NestedTicketDto(element));
         }
         this.createdOn = entity.getCreatedOn();
         this.cancellationCode = entity.getCancellationCode();
         this.contactEmail = entity.getContactEmail();
         this.performance = new NestedPerformanceDto(entity.getPerformance());
      }
   }

   public Booking fromDTO(Booking entity, EntityManager em) {
      if (entity == null) {
         entity = new Booking();
      }
      Iterator<Ticket> iterTickets = entity.getTickets().iterator();
      while (iterTickets.hasNext()) {
         boolean found = false;
         Ticket ticket = iterTickets.next();
         Iterator<NestedTicketDto> iterDtoTickets = this.getTickets()
               .iterator();
         while (iterDtoTickets.hasNext()) {
            NestedTicketDto dtoTicket = iterDtoTickets.next();
            if (dtoTicket.getId().equals(ticket.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            iterTickets.remove();
         }
      }
      Iterator<NestedTicketDto> iterDtoTickets = this.getTickets().iterator();
      while (iterDtoTickets.hasNext()) {
         boolean found = false;
         NestedTicketDto dtoTicket = iterDtoTickets.next();
         iterTickets = entity.getTickets().iterator();
         while (iterTickets.hasNext()) {
            Ticket ticket = iterTickets.next();
            if (dtoTicket.getId().equals(ticket.getId())) {
               found = true;
               break;
            }
         }
         if (found == false) {
            Iterator<Ticket> resultIter = em
                  .createQuery("SELECT DISTINCT t FROM Ticket t",
                        Ticket.class).getResultList().iterator();
            while (resultIter.hasNext()) {
               Ticket result = resultIter.next();
               if (result.getId().equals(dtoTicket.getId())) {
                  entity.getTickets().add(result);
                  break;
               }
            }
         }
      }
      entity.setCreatedOn(this.createdOn);
      entity.setCancellationCode(this.cancellationCode);
      entity.setContactEmail(this.contactEmail);
      if (this.performance != null) {
         entity.setPerformance(this.performance.fromDTO(
               entity.getPerformance(), em));
      }
      entity = em.merge(entity);
      return entity;
   }

   public float getTotalTicketPrice() {
      return this.totalTicketPrice;
   }

   public void setTotalTicketPrice(final float totalTicketPrice) {
      this.totalTicketPrice = totalTicketPrice;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public Set<NestedTicketDto> getTickets() {
      return this.tickets;
   }

   public void setTickets(final Set<NestedTicketDto> tickets) {
      this.tickets = tickets;
   }

   public Date getCreatedOn() {
      return this.createdOn;
   }

   public void setCreatedOn(final Date createdOn) {
      this.createdOn = createdOn;
   }

   public String getCancellationCode() {
      return this.cancellationCode;
   }

   public void setCancellationCode(final String cancellationCode) {
      this.cancellationCode = cancellationCode;
   }

   public String getContactEmail() {
      return this.contactEmail;
   }

   public void setContactEmail(final String contactEmail) {
      this.contactEmail = contactEmail;
   }

   public NestedPerformanceDto getPerformance() {
      return this.performance;
   }

   public void setPerformance(final NestedPerformanceDto performance) {
      this.performance = performance;
   }
   
}
