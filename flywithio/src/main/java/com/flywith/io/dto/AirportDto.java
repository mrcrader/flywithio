package com.flywith.io.dto;

import javax.persistence.EntityManager;

import com.flywith.io.model.Airport;
import com.flywith.io.dto.AddressDto;

public class AirportDto {
	
	private Long id;
	private String name;
	private IataCodeDto iataCode;
	private AddressDto address;
	
	public AirportDto() {
		
	}

	public AirportDto(Airport entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.iataCode = new IataCodeDto(entity.getIataCode());
		this.address = new AddressDto(entity.getAddress());
	}
	
	public Airport fromDto(Airport entity, EntityManager em) {
		if(entity == null) {
			entity = new Airport();
		}
		if(this.iataCode != null) {
			entity.setIataCode(( this.iataCode).fromDto(entity.getIataCode(),
					em));
		}
		if(this.address != null) {
			entity.setAddress(( this.address).fromDTO(entity.getAddress(),
					em));
		}
		entity.setName(this.name);
		entity = em.merge(entity);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public IataCodeDto getIataCode() {
		return iataCode;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIataCode(IataCodeDto iataCode) {
		this.iataCode = iataCode;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}
	
}
