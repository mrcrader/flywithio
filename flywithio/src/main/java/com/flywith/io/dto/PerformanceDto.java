package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.dto.NestedShowDto;
import com.flywith.io.model.Performance;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@SuppressWarnings("serial")
public class PerformanceDto implements Serializable {

   private Long id;
   private NestedShowDto show;
   private Date date;
   private String displayTitle;

   public PerformanceDto() {
	   
   }

   public PerformanceDto(final Performance entity) {
      if (entity != null) {
         this.id = entity.getId();
         this.show = new NestedShowDto(entity.getShow());
         this.date = entity.getDate();
         this.displayTitle = entity.toString();
      }
   }

   public Performance fromDTO(Performance entity, EntityManager em) {
      if (entity == null) {
         entity = new Performance();
      }
      if (this.show != null) {
         entity.setShow(this.show.fromDTO(entity.getShow(), em));
      }
      entity.setDate(this.date);
      entity = em.merge(entity);
      return entity;
   }

   public Long getId() {
      return this.id;
   }

   public void setId(final Long id) {
      this.id = id;
   }

   public NestedShowDto getShow() {
      return this.show;
   }

   public void setShow(final NestedShowDto show) {
      this.show = show;
   }

   public Date getDate() {
      return this.date;
   }

   public void setDate(final Date date) {
      this.date = date;
   }

   public String getDisplayTitle() {
      return this.displayTitle;
   }
   
}
