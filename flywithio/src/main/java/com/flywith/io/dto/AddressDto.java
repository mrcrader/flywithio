package com.flywith.io.dto;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.flywith.io.model.Address;

/**
 * 
 * @author IBA Group
 * @since 2018
 *
 */

@SuppressWarnings("serial")
public class AddressDto implements Serializable {

   private String street;
   private String city;
   private String country;

   public AddressDto() {
	   
   }

   public AddressDto(final Address entity) {
      if (entity != null) {
         this.street = entity.getStreet();
         this.city = entity.getCity();
         this.country = entity.getCountry();
      }
   }

   public Address fromDTO(Address entity, EntityManager em) {
      if (entity == null) {
         entity = new Address();
      }
      entity.setStreet(this.street);
      entity.setCity(this.city);
      entity.setCountry(this.country);
      return entity;
   }

   public String getStreet() {
      return this.street;
   }

   public void setStreet(final String street) {
      this.street = street;
   }

   public String getCity() {
      return this.city;
   }

   public void setCity(final String city) {
      this.city = city;
   }

   public String getCountry() {
      return this.country;
   }

   public void setCountry(final String country) {
      this.country = country;
   }
   
}
